This Repository consits in a Feedforward Neural Network designed to handle with a data from 
a https://www.kaggle.com/c/flavours-of-physics/data project. Once the program becomes trained
it has the capacity to predict values for new data. Each process has a total of 47 entries an one output.

The data is a recopilation of collision events composed of real mesaruments taken from LHCb detectors, observing collisions of accelerated particles with a 
specific mass range in which t decay cannot occur, this events correspond to backgroud data (labeled with 0), also there simulated data that correspong to signal events
of the tau decay specifically t-> 3muon,(labeled as 1).

Saved in a .csv file, our code is capable to train and identify tau decay of in new measuments.


The data is stored in the cluster in the carpet "data" of the user "avopeac" with the filename "trainig.csv".
