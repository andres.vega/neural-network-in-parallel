"""This code consists in the building of a Feedforward Neural Network in order
to perform predictions over https://www.kaggle.com/c/flavours-of-physics/data"""

import csv
from time import time
from mpi4py import MPI
import numpy as np

WORLD = MPI.COMM_WORLD
RANK = WORLD.Get_rank()
SIZE = WORLD.Get_size()

INITIAL_TIME = time()
#-------------------------------------------------------------------------------
def opener(number):
    """Here we are creating a function that allow us to
    read data from kaggle project, number will describe
    the total data we are going to retrieve from the
    kaggle files. It is doit in order to avoid problems
    in the code when scattering
    this matrix of data"""
    count = -1
    info = []
    with open('./training.csv', 'rb') as file:
        reader = csv.reader(file)
        for row in reader:
            count += 1
            if count > 0:
                part = []
                for item in row:
                    part.append(float(item))
                info.append(part)
    info = np.array(info)[0:number, :]
    return info

#------------------------------------------------------------------------------
def sigmoid(varx):
    """Here we define the activation function which is
    used in the Feedforward of our Neuran Network"""
    return 1 / (1 + np.exp(-varx))


def sigmoid_derivative(varx):
    """Derivative of our activation function will be
    used in order to minimize our loss function in the
    Backpropagation process"""
    return varx * (1.0 - varx)


class NeuralNetwork:
    """Class NN will contain the neccesary information
    of how they layers interact, First a feedforward process,
    and then a backpropagation process"""
    def __init__(self, inputi, out):
        self.input = inputi
        self.weights1 = np.random.rand(self.input.shape[1], 4)
        self.weights2 = np.random.rand(4, 1)
        self.yvar = out
        self.output = np.zeros(self.yvar.shape)
        self.layer1 = []


    def feedforward(self):
        """In this process our input data will be manipulated
        by the NN in order to get an output reult"""
        self.layer1 = sigmoid(np.dot(self.input, self.weights1))
        self.output = sigmoid(np.dot(self.layer1, self.weights2))


    def backprop(self):
        """backpropagation process will calculate the loss
        function and will try to minimize the error, for
        this purpose it will change constantly the set
        weights until the epochs train it enough well
        to our outpus coincides with the desire answers"""
        d_weights2 = np.dot(self.layer1.T,
                            (2 * (self.yvar - self.output)
                             * sigmoid_derivative(self.output)))
        d_weights1 = np.dot(self.input.T,
                            (np.dot(2 * (self.yvar - self.output)
                                    * sigmoid_derivative(self.output),
                                    self.weights2.T)
                             * sigmoid_derivative(self.layer1)))
        self.weights1 += d_weights1
        self.weights2 += d_weights2

#-------------------------------------------------------------------------------
def training():
    """This is the training function, this will work
    later to sharp the weight values in our NN"""
    for epoch in range(1500):
        NN.feedforward()
        NN.backprop()
        if epoch == -1:
            print 'something goes wrong'
    return NN.output
#-------------------------------------------------------------------------------
NUMBER = 19200
RECVDATA = np.empty((NUMBER / SIZE, 51), dtype=np.float64)
INFO = None

if RANK == 0:
    print('     Id     ', 'Obtained trained values')
    INFO = opener(NUMBER)
WORLD.Scatter(INFO, RECVDATA, root=0)
#print RECVDATA.shape ,'in RANK =', RANK
#-------------------------------------------------------------------------------
X = RECVDATA[0:1, 1:47]
Y = RECVDATA[0:1, 50:51]
NN = NeuralNetwork(X, Y)
#print NN.weights1.shape
EXT = []
RECVDATA1 = None
RECVDATA2 = None

for i in range(RECVDATA.shape[0]):
    if i > 0:
        NN.weights1 = RECVDATA1/SIZE
        NN.weights2 = RECVDATA2/SIZE

    NN.input = RECVDATA[i:i+1, 1:47]
    NN.yvar = RECVDATA[i:i+1, 50:51]
    print(float(RECVDATA[i:i+1, 0:1]), '|  ', float(training()))
    EXT.append([float(RECVDATA[i:i+1, 0:1]), float(training())])

    RECVDATA1 = np.empty(NN.weights1.shape, dtype=np.float64)
#    print recvdata1.shape
    RECVDATA2 = np.empty(NN.weights2.shape, dtype=np.float64)
#    nn.weights1.shape
    WORLD.Allreduce(NN.weights1, RECVDATA1)
    WORLD.Allreduce(NN.weights2, RECVDATA2)

#print ext
END_TIME = time()
if RANK == 0:
    print 'execution time with: ', SIZE, 'was : ', END_TIME - INITIAL_TIME
