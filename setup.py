from distutils.core import setup

setup(name='neuralpy',
      version='1.0',
      description='Feedforward Neural Network in Parallel Computing',
      maintainer='Daniel Darquea',
      maintainer_email='daniel.darquea@yachaytech.edu.ec',
      developer='Andrés Vega Alcívar',
      developer_email='andres.vegak@yachaytech.edu.ec',
      url='gitlab.com/andres.vega/neural-network-in-parallel',
      platforms=['unix'],
      scripts=['bin/neuralpy'],
      packages=['neural'])
